// Lab_10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

void Philosophers()
{
	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	TCHAR comLine[MAX_PATH];
	_stprintf_s(comLine, _T("Philosopher.exe"));
	CreateProcess(0, comLine, 0, 0, 0, 0, 0, 0, &si, &pi);
}

void ReaderWriter()
{
	TCHAR* rMutex = _T("rMutex");
	TCHAR* wMutex = _T("wMutex");
	TCHAR* rSemaphore = _T("readersSemaphore");
	DWORD SEMAPHORE_MAX = 1000;
	TCHAR* cMap = _T("counterMapping");
	TCHAR* nMap = _T("newsMapping");

	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	TCHAR comLine[MAX_PATH];
	_stprintf_s(comLine, _T("ReaderWriter.exe %s %s %s %d %s, %s"), wMutex, rMutex, rSemaphore, SEMAPHORE_MAX, cMap, nMap);
	CreateProcess(0, comLine, 0, 0, 0, 0, 0, 0, &si, &pi);
}

void MultipleReadWrite()
{
	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	TCHAR comLine[MAX_PATH];
	_stprintf_s(comLine, _T("MultipleReadWrite.exe"));
	CreateProcess(0, comLine, 0, 0, 0, 0, 0, 0, &si, &pi);
}

int _tmain(int argc, _TCHAR* argv[])
{
	//Philosophers();
	//ReaderWriter();
	MultipleReadWrite();

	//getchar();
	return 0;
}

