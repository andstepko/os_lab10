// ReaderWriter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

#define MyCreateThread(func, parametr) CreateThread(0, 0, func, parametr, 0, 0);

DWORD SEMAPHORE_MAX;
const DWORD readerSleep = 5000;
const DWORD writerSleep = 5000;

HANDLE hWriterMutex;
HANDLE hReaderMutex;
HANDLE hReadersSemaphore;
CRITICAL_SECTION printSection;

LPVOID pCounterBuf;
LPVOID pNewsBuf;

DWORD* countMem;
TCHAR* newsMem;
//TCHAR* _news;

void Print(TCHAR* text)
{
	EnterCriticalSection(&printSection);
	_tprintf(text);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
//void Print(TCHAR* text, DWORD num1)
//{
//	EnterCriticalSection(&printSection);
//	_tprintf(text, num1);
//	_tprintf(_T("\n"));
//	LeaveCriticalSection(&printSection);
//}
void Print(TCHAR* text, LONG num1)
{
	EnterCriticalSection(&printSection);
	_tprintf(text, num1);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
void Print(TCHAR* text, DWORD num1, DWORD num2)
{
	EnterCriticalSection(&printSection);
	_tprintf(text, num1, num2);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}

DWORD WINAPI writerFun(LPVOID p)
{
	TCHAR temp[10];

	while(1)
	{
		//Print(_T("Writer."));
		// Occupy the writerMutex.
		if(WaitForSingleObject(hWriterMutex, INFINITE) != WAIT_OBJECT_0)
		{
			Print(_T("ERROR waiting for writerMutex!!!"));
		}
		// Wait for all readers to stop reading.
		if(WaitForSingleObject(hReaderMutex, INFINITE) != WAIT_OBJECT_0)
		{
			Print(_T("ERROR waiting for readerMutex!!!"));
		}
		ReleaseMutex(hReaderMutex);
		// Writing.
		_stprintf_s(temp, _T("news(%d)\n"), countMem[0]);

		//_news[countMem[0]] = temp;
		DWORD index = 0;
		while(newsMem[index] != '\0')
		{
			index++;
			//Print(_T("Index++"));
		}
		newsMem += index;
		
		_stprintf_s(newsMem, _tcslen(temp) * sizeof(TCHAR), temp);

		newsMem -= index;

		Print(_T("WRITER (%d)"), countMem[0]);

		//Print(newsMem);
		
		//newsCount++;
		countMem[0] = countMem[0] + 1;

		ReleaseMutex(hWriterMutex);

		Sleep(writerSleep);
	}

	return 0;
}

DWORD WINAPI readerFun(void*)
{
	LONG semaphoreCount;

	while(1)
	{
		//Print(_T("Reader."));
		// Wait while a writer finishes.
		if(WaitForSingleObject(hWriterMutex, INFINITE) != WAIT_OBJECT_0)
		{
			Print(_T("ERROR!!!"));
		}
		ReleaseMutex(hWriterMutex);
		// Block readerMutex if it isn't already.
		WaitForSingleObject(hReaderMutex, 1);
		// Decrease the semaphore count.
		WaitForSingleObject(hReadersSemaphore, INFINITE);
		// Reading.
		//Print(_T("count[0] == (%d)"), countMem[0]);
		if(countMem[0] > 0)
		{
			//Print(_news[countMem[0] - 1]);
			DWORD index = 0;
			while(newsMem[index] != '\0')
			{
				index++;
				//Print(_T("Index++"));
			}
			index -= 2;
			while((index > 0) && (newsMem[index] != '\n'))
			{
				index--;
				//Print(_T("Index--"));
			}
			if(index > 0)
				index++;
			//Print(_T("Index == (%d)"), index);
			newsMem += index;
			//Print(_T("AAA"));
			Print(newsMem);
			newsMem -= index;
		}
		else
		{
			Print(_T("nothing"));
		}
		// Increase semaphore count back.
		
		ReleaseSemaphore(hReadersSemaphore, 1, &semaphoreCount);
		// Release readerMutex if I'm the last reading reader.
		//Print(_T("semaphoreCount == (%ld)"), semaphoreCount);
		if(semaphoreCount == SEMAPHORE_MAX - 1)
		{
			ReleaseMutex(hReaderMutex);
		}
		Sleep(readerSleep);
	}

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	InitializeCriticalSection(&printSection);

	TCHAR* wMutex = argv[1];
	TCHAR* rMutex = argv[2];
	TCHAR* rSemaphore = argv[3];
	SEMAPHORE_MAX = (DWORD)_ttoi(argv[4]);
	TCHAR* cMap = argv[5];
	TCHAR* nMap = argv[6];
	DWORD readerCount = (DWORD)_ttoi(argv[7]);
	DWORD writerCount = (DWORD)_ttoi(argv[8]);

	/*Print(wMutex);
	Print(rMutex);
	Print(rSemaphore);
	Print(_T("DWORD == (%d)"), SEMAPHORE_MAX);
	Print(cMap);
	Print(nMap);
	Print(_T("readerCount == (%d)"), readerCount);
	Print(_T("writerCount == (%d)"), writerCount);*/
	
	hWriterMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, wMutex);
	hReaderMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, rMutex);
	hReadersSemaphore =  OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, rSemaphore);

	HANDLE hCountMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, cMap);
	if (hCountMap == NULL)
	{
		_tprintf(_T("Could not open file countMapping object (%d).\n"), GetLastError());
		return 1;
	}
	HANDLE hNewsMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, nMap);
	if (hNewsMap == NULL)
	{
		_tprintf(_T("Could not open file newsMapping object (%d).\n"), GetLastError());
		return 1;
	}
	pCounterBuf = MapViewOfFile(hCountMap, // handle to map object
							FILE_MAP_ALL_ACCESS, // read/write permission
							0,
							0,
							sizeof(DWORD));
	if (pCounterBuf == NULL)
	{
		_tprintf(_T("Could not countMap view of file (%d).\n"), GetLastError());
		CloseHandle(hCountMap);
		return 1;
	}
	pNewsBuf = MapViewOfFile(hNewsMap, // handle to map object
							FILE_MAP_ALL_ACCESS, // read/write permission
							0,
							0,
							8000);
	if (pNewsBuf == NULL)
	{
		_tprintf(_T("Could not countMap view of file (%d).\n"), GetLastError());
		CloseHandle(hNewsMap);
		return 1;
	}
	countMem = (DWORD*) pCounterBuf;

	newsMem = (TCHAR*) pNewsBuf;

	for(int i = 0; i < writerCount; i++)
	{
		MyCreateThread(writerFun, (void*)0);
	}
	for(int i = 0; i < readerCount; i++)
	{
		MyCreateThread(readerFun, 0);
	}
	//MyCreateThread(readerFun, 0);

	getchar();
	return 0;
}