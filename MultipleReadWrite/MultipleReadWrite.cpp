// MultipleReadWrite.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

TCHAR* rMutex = _T("readersMutex");
TCHAR* wMutex = _T("writerMutex");
TCHAR* rSemaphore = _T("readersSemaphore");
DWORD SEMAPHORE_MAX = 1000;
TCHAR* cMap = _T("counterMapping");
TCHAR* nMap = _T("newsMapping");

void CreateReadWrite(DWORD writeCount, DWORD readCount)
{
	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	TCHAR comLine[MAX_PATH];
	_stprintf_s(comLine, _T("ReaderWriter.exe %s %s %s %d %s %s %d %d"), wMutex, rMutex, rSemaphore, SEMAPHORE_MAX,
		cMap, nMap, writeCount, readCount);
	CreateProcess(0, comLine, 0, 0, 0, 0, 0, 0, &si, &pi);
}

int _tmain(int argc, _TCHAR* argv[])
{
	/*TCHAR* rMutex = _T("readersMutex");
	TCHAR* wMutex = _T("writerMutex");
	TCHAR* rSemaphore = _T("readersSemaphore");
	DWORD SEMAPHORE_MAX = 1000;
	TCHAR* cMap = _T("counterMapping");
	TCHAR* nMap = _T("newsMapping");*/

	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	DWORD timesToRunTimer = 5;

	//TCHAR comLine[MAX_PATH];

	HANDLE hWriterMutex = CreateMutex(0, FALSE, wMutex);
	HANDLE hReaderMutex = CreateMutex(0, FALSE, rMutex);
	HANDLE hReadersSemaphore = CreateSemaphore(0, SEMAPHORE_MAX, SEMAPHORE_MAX, rSemaphore);

	CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, 0, 4, cMap);
	CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, 0, 8000, nMap);

	//_stprintf_s(comLine, _T("ReaderWriter.exe %s %s %s %d %s %s %d %d"), wMutex, rMutex, rSemaphore, SEMAPHORE_MAX, cMap, nMap);
	
	HANDLE hTimer = CreateWaitableTimer(0, TRUE, 0);
	if(hTimer == NULL)
	{
		_tprintf(_T("Error creating waitableTimer (%d)."), GetLastError());
		return 1;
	}
	LARGE_INTEGER time;
	time.QuadPart = -70000000;

	for(int i = 0; i < timesToRunTimer; i++)
	{
		_tprintf(_T("TIMER ELAPSED (%d)\n"), (DWORD)i);
		CreateReadWrite(1, 1);
		if(!SetWaitableTimer(hTimer, &time, 5000, NULL, NULL, 0))
		{
			_tprintf(_T("Error settting waitableTimer (%d)."), GetLastError());
			return 1;
		}

		if(WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0)
		{
			_tprintf(_T("Error waiting waitableTimer (%d)."), GetLastError());
		}

		WaitForSingleObject(hTimer, INFINITE);
	}

	getchar();
	return 0;
}

