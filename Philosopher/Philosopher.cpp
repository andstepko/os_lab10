// Philosopher.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

#define MyCreateThread(func, parametr) CreateThread(0, 0, func, parametr, 0, 0);

TCHAR* sticksFamily = _T("stick");
DWORD PHILOSOPHER_NUM = 6;
DWORD MEALS = 5;
HANDLE* Mutexes;
BOOL* bMutexes;
CRITICAL_SECTION printSection;
CRITICAL_SECTION logSection;
TCHAR text[MAX_PATH];

struct PhilInfo
{
	DWORD Index;
	DWORD TimeThink;
	DWORD TimeEat;
	//DWORD TimeOccupation;
	DWORD MealNum;
	DWORD philNum;
};

DWORD GetLeftStick(DWORD philIndex, DWORD totalPhil)
{
	if(philIndex < totalPhil - 1)
	{return philIndex + 1;}
	else
	{return 0;}
}

//void LockStick(DWORD index)
//{
//	if(index < PHILOSOPHER_NUM)
//		bMutexes[index] = TRUE;
//}
//void UnLockStick(DWORD index)
//{
//	if(index < PHILOSOPHER_NUM)
//		bMutexes[index] = FALSE;
//}

void Print(TCHAR* text)
{
	EnterCriticalSection(&printSection);
	_tprintf(text);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
void Print(TCHAR* text, DWORD num1)
{
	EnterCriticalSection(&printSection);
	_tprintf(text, num1);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
void Print(TCHAR* text, DWORD num1, DWORD num2)
{
	EnterCriticalSection(&printSection);
	_tprintf(text, num1, num2);
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}

void LogState()
{
	EnterCriticalSection(&printSection);
	for(int i = 0; i < PHILOSOPHER_NUM; i++)
	{
		if(bMutexes[i])
		{
			_tprintf(_T(". (%d) "), (DWORD)i);
		}
		else
		{
			_tprintf(_T("| (%d) "), (DWORD)i);
		}
	}
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
void LogState(TCHAR* prefix)
{
	EnterCriticalSection(&printSection);
	_tprintf(prefix);
	for(int i = 0; i < PHILOSOPHER_NUM; i++)
	{
		if(bMutexes[i])
		{
			_tprintf(_T(". (%d) "), (DWORD)i);
		}
		else
		{
			_tprintf(_T("| (%d) "), (DWORD)i);
		}
	}
	_tprintf(_T("\n"));
	LeaveCriticalSection(&printSection);
}
void LogState(TCHAR* prefix, DWORD philIndex)
{
	EnterCriticalSection(&printSection);
	_tprintf(prefix);
	for(int i = 0; i < PHILOSOPHER_NUM; i++)
	{
		if(bMutexes[i])
		{
			_tprintf(_T(". (%d) "), (DWORD)i);
		}
		else
		{
			_tprintf(_T("| (%d) "), (DWORD)i);
		}
	}
	_tprintf(_T("        (%d)\n"), philIndex);
	LeaveCriticalSection(&printSection);
}
void ChangeLogState(BOOL occupation, DWORD stickLeft, DWORD stickRight)
{
	EnterCriticalSection(&logSection);
	bMutexes[stickLeft] = occupation;
	bMutexes[stickRight] = occupation;
	if(occupation)
		LogState(_T("._"), stickRight);
	else
		LogState(_T("|_"), stickRight);
	LeaveCriticalSection(&logSection);
}

DWORD WINAPI philFun(LPVOID parametr)
{
	PhilInfo phInfo = *(PhilInfo*)parametr;
	DWORD waitingResult;
	DWORD rightIndex = phInfo.Index;
	DWORD leftIndex = GetLeftStick(phInfo.Index, phInfo.philNum);
	DWORD count = 0;

	TCHAR rightMutex[8];
	_stprintf_s(rightMutex, _T("%s%d"), sticksFamily, rightIndex);
	TCHAR leftMutex[8];
	_stprintf_s(leftMutex, _T("%s%d"), sticksFamily, leftIndex);

	while(count < MEALS)
	{
		// Thinking.
		//Print(_T("(%d)Started thinking..."), phInfo.Index);
		Sleep(phInfo.TimeThink);
		//Print(_T("(%d)Finished thinking."), phInfo.Index);

		// Occupying sticks
		HANDLE temp[2] = {Mutexes[leftIndex], Mutexes[rightIndex]};
		waitingResult = WaitForMultipleObjects(2, temp, TRUE, INFINITE);
		
		ChangeLogState(TRUE, leftIndex, rightIndex);


		// Both sticks were successfully occupied.
		// Eating.
		//Print(_T("(%d)Started eating..."), phInfo.Index);
		Sleep(phInfo.TimeEat);
		//Print(_T("(%d)Finished eating."), phInfo.Index);
		
		// Releasing sticks.
		
		ChangeLogState(FALSE, leftIndex, rightIndex);

		ReleaseMutex(Mutexes[rightIndex]);
		ReleaseMutex(Mutexes[leftIndex]);

		count++;
	}

	return 1;
}

HANDLE* DecalreMutexes(DWORD howMany)
{
	HANDLE* result = new HANDLE[howMany];

	for(int i = 0; i < howMany; i++)
	{
		TCHAR mutexName[8];
		_stprintf_s(mutexName, _T("%s%d"), sticksFamily, (DWORD)i);
		result[i] = CreateMutex(0, FALSE, mutexName);
		if((result[i] == INVALID_HANDLE_VALUE) || (result[i] == NULL))
		{
			_tprintf(_T("Error creating mutex No_%d (%d).\n"), (DWORD)i, GetLastError());
			return NULL;
		}
	}
	return result;
}
//HANDLE DecalreFlagsMutex()
//{
//	HANDLE result;
//	TCHAR* mutexName = _T("flags");
//
//	result = CreateMutex(0, FALSE, mutexName);
//	if((result == INVALID_HANDLE_VALUE) || (result == NULL))
//	{
//		_tprintf(_T("Error creating flagsMutex (%d).\n"), GetLastError());
//		return NULL;
//	}
//	return result;
//}

HANDLE StartPhil(DWORD philIndex, DWORD timeThink, DWORD timeEat, DWORD mealNum, DWORD philNum)
{
	PhilInfo *phInfoP = new PhilInfo;
	PhilInfo phI = {philIndex, timeThink, timeEat, mealNum, philNum};
	phInfoP[0] = phI;
	return  MyCreateThread(philFun, (void*)phInfoP);
}

int _tmain(int argc, _TCHAR* argv[])
{
	DWORD timeToThink = 10000;
	DWORD timeToEat = 8000;

	int minThink = 5000;
	int maxThink = 10000;
	int minEat = 3000;
	int maxEat = 8000;

	//_tprintf(_T("Started tmain of Philosopher.\n"));
	InitializeCriticalSection(&printSection);
	InitializeCriticalSection(&logSection);
	// Declare Mutexes.
	Mutexes = DecalreMutexes(PHILOSOPHER_NUM);
	bMutexes = new BOOL[PHILOSOPHER_NUM];
	for(int i = 0; i < PHILOSOPHER_NUM; i++)
	{
		bMutexes[i] = FALSE;
	}
	//hFlagsMutex = DecalreFlagsMutex();
	LogState();

	// Create Philosophers.
	for(int i = 0; i < PHILOSOPHER_NUM; i++)
	{
		//StartPhil(i, timeToThink, timeToEat, MEALS, PHILOSOPHER_NUM);
		StartPhil(i, minThink + rand() % ( maxThink-minThink ), minEat + rand() % ( maxEat-minEat ), MEALS, PHILOSOPHER_NUM);
	}
	//StartPhil(0, timeToThink, timeToEat, MEALS, PHILOSOPHER_NUM);
	//StartPhil(1, timeToThink, timeToEat, MEALS, PHILOSOPHER_NUM);

	getchar();
	return 0;
}